import { useState, useEffect, useContext } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import { Card, Button, Container, InputGroup, FormControl, Form } from 'react-bootstrap';
import { add, total, list, get, exists, remove, quantity, update  } from 'cart-localstorage'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function SpecificProduct(){

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [counter, setCounter] = useState(1)
	let cart = []

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const history = useHistory();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])


const addToCart = () => {
	const thisProduct = {id: productId, name: name, price: price}
		add(thisProduct, counter)
}
	
	const subtract = () => {
		setCounter(counter - 1)
	}


	const counterInput = (value) => {
		if(value === ''){
			value = 1
		}else if(value === "0"){
			alert("Quantity can't be lower than 1.")
			value = 1
		}
		setCounter(value)
	}

	return(
		<Container>
		<Card className="mt-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<h6>Price: {price}</h6>
<Container className="col-lg-2">
	<InputGroup className="mb-3 lg">
	{counter === 1 ? <Button variant="outline-secondary" id="button-addon1" disabled>
      --
    </Button> :
    <Button variant="outline-secondary" id="button-addon1" onClick={subtract}>
      --
    </Button>}
    <Form.Control className="text-center"
		type="number"
		value={counter}
		onChange={e => counterInput(e.target.value)}
		required
    />
    <Button variant="outline-secondary" id="button-addon1" onClick={() => setCounter(counter + 1)}>
      +</Button>
  </InputGroup>	
</Container>
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				{user.id !== null ?
					<Button variant="primary" block="true" onClick={addToCart}>Add to Cart</Button>
					:
					<Link className="btn btn-danger" to="/login">Log In to Checkout</Link>
				}
			</Card.Footer>
		</Card>
		</Container>
	)
}

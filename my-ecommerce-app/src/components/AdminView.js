import { useState, useEffect } from 'react'
import {  Table, Button, Modal, Form, Container } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function AdminView(props){

	const [productsArr, setProductsArr ] = useState([])

	const { productsProp, fetchData} = props
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	//Functions to handle opening and closing modals
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)
	const openEdit = (productId) => {
		// fetch request with the course's ID
		// populates the edit modal's input fields with the proper information
		setShowEdit(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id)
			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)

		})
	}
	const closeEdit = () => {
		setShowEdit(false)
					setName("")
					setDescription("")
					setPrice(0)}

const editProduct = (e) => {
	e.preventDefault()
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully Updated"})

					fetchData()
					closeEdit()

			}else{
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

			}
		})

	}



	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: `Product successfully ${bool}`})

			}else{
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

			}
		})

	}

	useEffect(() => {
		const products = productsProp.map(product => {
			return (
				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						{product.isActive ? <span> Available </span> : <span> Unavailable </span>}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button> {''}
						{product.isActive ? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button> : 
						<Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>}
					</td>
				</tr>
				)
		})

		setProductsArr(products)

	}, [productsProp])



		const addProduct = (e) => {
			e.preventDefault()
			fetch(`${process.env.REACT_APP_API_URL}/products`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data){


					Swal.fire({
						title: "You Successfully added a New Product",
						icon: "success",
						text: "New Product Added"
						})

					fetchData()
					closeAdd()
					setName("")
					setDescription("")
					setPrice(0)

				}else{
					fetchData()
					Swal.fire({
						title: "Something went Wrong",
						icon: "error",
						text: "Please try again"
						})					
				}
})
		}

useEffect(() => {
	return (
		fetchData()

		)
}, [name, description, price])



	return(
		<Container>
		<div className="my-4 text-center">
			<h2>Admin Dashboard</h2>
			<Button variant="primary" onClick={openAdd}>Add New Product</Button>
		</div>
		<Table striped bordered hover responsive="lg">
			<thead className="bg-dark text-white">
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Availability</th>
					<th></th>
				</tr>
			</thead>
			<tbody>{productsArr}</tbody>
		</Table>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={a => addProduct(a)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text"
								placeholder="Enter Product name"
								value={name}
								onChange={e => setName(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text"
								placeholder="Enter Description"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number"
								placeholder="Enter Price"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text"
								placeholder="Enter product name"
								value={name}
								onChange={e => setName(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text"
								placeholder="Enter description"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number"
								placeholder="Enter price"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</Container>
		)
}
import { Row, Col, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
 
export default function Banner({bannerProps}){
	const { title, content, destination, label } = bannerProps
	return(

		<Row>
			<Container id="bannerBg">
			<Col className="p-5 text-center">
				<h1> {title}</h1>
				<p id="sub"> {content}</p>
				<Link className="btn btn-primary" to={destination}> {label}</Link>

			</Col>
			</Container>
		</Row>

		)
}